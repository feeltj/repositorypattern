<?php

namespace App\Traits\Eloquent;

use Illuminate\Database\Eloquent\Builder;

trait hasLive
{
   public function scopeLive(Builder $builder)
   {
       return $builder->where('live', true);
   }
}
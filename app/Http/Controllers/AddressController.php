<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\UserRepository;
use App\Repositories\Contracts\AddressRepository;

class AddressController extends Controller
{
	protected $users;

	protected $addresses;

	public function __construct(UserRepository $users, AddressRepository $addresses)
	{ 
		$this->users = $users;
		$this->addresses = $addresses;
	}
    public function index()
    {
        $this->addresses->delete(1);
    }
}
